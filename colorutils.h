#ifndef COLORUTILS_H_
#define COLORUTILS_H_

int get_color_pair(int foreground, int background);
int initialize_all_color_pairs();
#endif
