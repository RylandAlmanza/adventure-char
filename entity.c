#include "entity.h"

void destroy_Entity(Entity *entity) {
    free(entity->cells);
}

Entity create_small_tree(int x, int y) {
    Entity tree = {.x = x, .y = y, .width = 3, .height = 2};

    tree.cells = malloc(sizeof(Cell) * 4);
    Cell cell0 = {.x = 0,
                  .y = 0,
                  .z = 4,
                  .character = '(',
                  .foreground = COLOR_GREEN,
                  .background = -1};
    Cell cell1 = {.x = 1,
                  .y = 0,
                  .z = 4,
                  .character = '~',
                  .foreground = COLOR_GREEN,
                  .background = -1};
    Cell cell2 = {.x = 2,
                  .y = 0,
                  .z = 4,
                  .character = ')',
                  .foreground = COLOR_GREEN,
                  .background = -1};
    Cell cell3 = {.x = 1,
                  .y = 1,
                  .z = 1,
                  .character = '|',
                  .foreground = COLOR_GREEN,
                  .background = -1};
    tree.cells[0] = cell0;
    tree.cells[1] = cell1;
    tree.cells[2] = cell2;
    tree.cells[3] = cell3;
    tree.number_of_cells = 4;
    return tree;
}


Entity create_guy(int x, int y) {
    Entity guy = {.x = x, .y = y, .width = 1, .height = 2};
    guy.cells = malloc(sizeof(Cell) * 2);
    Cell cell0 = {.x = 0,
                  .y = 0,
                  .z = 2,
                  .character = 'o',
                  .foreground = COLOR_WHITE,
                  .background = -1};
    Cell cell1 = {.x = 0,
                  .y = 1,
                  .z = 1,
                  .character = 'X',
                  .foreground = COLOR_WHITE,
                  .background = -1};
    guy.cells[0] = cell0;
    guy.cells[1] = cell1;
    guy.number_of_cells = 2;
    return guy;
}

