#ifndef ENTITY_H_
#define ENTITY_H_

#include <ncurses.h>

#include "colorutils.h"
#include "cell.h"

typedef struct EntityS Entity;

struct EntityS {
  int x;
  int y;
  int width;
  int height;
  Cell *cells;
  int number_of_cells;
};

void destroy_Entity(Entity *entity);

Entity create_small_tree(int x, int y);
Entity create_guy(int x, int y);
#endif
